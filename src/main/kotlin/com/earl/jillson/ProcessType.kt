package com.earl.jillson

enum class ProcessType { SPLIT, COMBINE }

package com.earl.jillson

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming


@JsonNaming(
    PropertyNamingStrategies.SnakeCaseStrategy::class
)
data class User(
    val id: Int,
    val firstName: String,
    val lastName: String,
    var email: String,
    val gender: String,
    val ipAddress: String,
    val favoriteAnimal: String,
    val favoriteCarBrand: String,
    val favoriteColor: String,
    val favoriteDrug: String,
    val race: String,
    val country: String
)
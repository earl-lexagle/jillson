package com.earl.jillson

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.coroutines.launch
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlin.io.path.Path
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.pathString
import java.io.File
import java.util.Collections

class JSONFileProcessor(val pathOrDir: String, val outputDir: String) {

    suspend fun splitJSONFile() {
        val users = mutableListOf<User>()
        val mapper = jacksonObjectMapper()
        val jobs = mutableListOf<Job>()

        users.addAll(mapper.readValue(File(pathOrDir)))
        val groupMap = users.groupBy { it.country }

        coroutineScope {
            groupMap.keys.forEach {
                val job = this.launch(Dispatchers.IO) {
                    val serialized = mapper.writeValueAsString(groupMap[it])
                    writeToFile(serialized, it)
                }
                jobs.add(job)
            }
        }
        jobs.joinAll()
    }

    suspend fun combineJSONFiles() {
        val mapper = jacksonObjectMapper()
        val deferredUsers = Collections.synchronizedList(mutableListOf<Deferred<List<User>>>())

        coroutineScope {
            Path(pathOrDir).listDirectoryEntries().forEach {
                val deferred: Deferred<List<User>> = async(Dispatchers.IO) {
                    val users = mapper.readValue<List<User>>(File(it.pathString))
                    return@async users
                }
                deferredUsers.add(deferred)
            }
        }

        val users = deferredUsers.awaitAll().flatten()
        val serialized = mapper.writeValueAsString(users)
        writeToFile(serialized, System.currentTimeMillis().toString())
    }

    private fun writeToFile(contents: String, filename: String) {
        val file = File("$outputDir/$filename.json")
        file.writeText(contents)
    }
}
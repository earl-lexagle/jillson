import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.core.PrintHelpMessage
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.enum
import kotlin.system.exitProcess
import com.earl.jillson.JSONFileProcessor
import com.earl.jillson.ProcessType
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File

class Cli : CliktCommand() {
    private val currentDir = System.getProperty("user.dir")

    val pathOrDir: String by option(
        "-i",
        "--input",
        metavar = "input",
        help = "Path of JSON file or directory of the JSON files"
    ).default(
        currentDir
    ).check {
        val file = File(this.pathOrDir)
        if (this.type == ProcessType.SPLIT) {
            file.isFile()
        } else {
            file.isDirectory()
        }
    }
    val outputDir: String by option(
        "-o",
        "--output",
        metavar = "output",
        help = "Output directory of JSON file/s"
    ).default(currentDir)
    val type: ProcessType by argument(
        "split/combine",
        help = "Split/Combine"
    ).enum<ProcessType>(ignoreCase = true) { it.name.lowercase() }

    override fun run() = Unit
}

fun main(args: Array<String>) {
    val cli = Cli()

    val parsed = try {
        cli.parse(args)
        true
    } catch (e: CliktError) {
        cli.echoFormattedHelp(e)
        if (e !is PrintHelpMessage) {
            println("Unable to parse commands.")
        }
        false
    }

    if (!parsed) {
        exitProcess(-1)
    }

    println("Input directory: ${cli.pathOrDir}")
    println("Output directory: ${cli.outputDir}")
    println("Process selected: ${cli.type}")

    val jill = JSONFileProcessor(pathOrDir = cli.pathOrDir, outputDir = cli.outputDir)

    runBlocking {
        launch {
            when (cli.type) {
                ProcessType.SPLIT -> jill.splitJSONFile()
                ProcessType.COMBINE -> jill.combineJSONFiles()
            }
        }
    }

}
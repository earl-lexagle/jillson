# Jillson

- Combines multiple JSON user files to a single JSON file
- Splits a JSON user file to multiple JSON files grouped by country

## Prerequisites

- Java

## Usage

```bash
cd build/libs

# input and output files will be placed in DIR
export DIR=''

mkdir -p $DIR/split/in

cp 'replace with the pathname of the file' $DIR/split/in
# ex. cp ./test.json $DIR/split/in
mkdir -p $DIR/combine/in
cp -r 'replace with directory on where the files are and add /.' $DIR/combine/in
# ex. cp -r ./dir/. $DIR/combine/in

# splitting files
mkdir $DIR/split/out
java -jar jillson-1.0-SNAPSHOT.jar split --input=$DIR/split/in/$FILENAME.json --output=$DIR/split/out

# combining files
mkdir $DIR/combine/out
java -jar jillson-1.0-SNAPSHOT.jar combine --input=$DIR/combine/in --output=$DIR/combine/out
```